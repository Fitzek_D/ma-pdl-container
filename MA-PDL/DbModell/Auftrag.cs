﻿using System;
using System.Collections.Generic;

namespace MA_PDL.DbModell
{
    public class Auftrag
    {
        public int Id { get; set; }
        public int Auftragsnummer { get; set; }
        public string Bezeichnung { get; set; }
        public int Dauer { get; set; }

        public Kunde Kunde { get; set; }
        public ICollection<ZeitarbeiterAuftrag> Zeitarbeitauftraege { get; set; }
    }

}
