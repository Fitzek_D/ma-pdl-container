﻿using System;
namespace MA_PDL.DbModell
{
    public class Benutzer
    {
        public int Id { get; set; }
        public string Benutzername { get; set; }
        public string Name { get; set; }
        public string Rolle { get; set; }
        public string Passwort { get; set; }

        public virtual Kunde Kunde { get; set; }
        public int? KundeId { get; set; }
    }

}
