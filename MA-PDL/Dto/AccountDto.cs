﻿using System;
using System.Collections.Generic;

namespace MA_PDL.Dto
{
    public class AccountDto
    {
        public int Id { get; set; }
        public string Benutzername { get; set; }
        public string Rolle { get; set; }
        public bool IsValid { get; set; }
    }

}
