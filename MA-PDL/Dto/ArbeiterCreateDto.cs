﻿using System;
namespace MA_PDL.Dto
{
    public class ArbeiterCreateDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public int Gehalt { get; set; }
        public int UserId { get; set; }
    }
}
