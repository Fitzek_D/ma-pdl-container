﻿using System;
using System.Collections.Generic;
using System.Linq;
using MA_PDL.Db;
using MA_PDL.DbModell;
using MA_PDL.Dto;
using Microsoft.AspNetCore.Mvc;

namespace MA_PDL.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AuftragController : Controller
    {
        [HttpGet]
        [ActionName("get-auftraege")]
        public ActionResult<List<AuftragDto>> GetAuftraege([FromServices] Database db, int id)
        {

            if (id != 0)
            {
                var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == id);

                var auftraegeDb = db.Auftraege.ToList();

                var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                if (kunde.Auftraege != null)
                {
                    var auftraege = kunde.Auftraege.ToList();

                    var auftragliste = new List<AuftragDto>();

                    foreach (var auftrag in auftraege)
                    {
                        var auftragDto = new AuftragDto
                        {
                            Nummer = auftrag.Auftragsnummer,
                            Bezeichnung = auftrag.Bezeichnung,
                            Dauer = auftrag.Dauer,
                            KundeId = kunde.Id
                        };

                        auftragliste.Add(auftragDto);
                    }

                    return Ok(auftragliste);
                }
            }

            var fakeauftraege = new List<AuftragDto>();
            return Ok(fakeauftraege);
        }

        [HttpPost]
        [ActionName("add-auftrag")]
        public ActionResult AddAuftrag([FromBody] AuftragCreateDto newAuftrag, [FromServices] Database db)
        {
            var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == newAuftrag.UserId);

            var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

            var auftrag = new Auftrag
            {
                Bezeichnung = newAuftrag.Bezeichnung,
                Dauer = newAuftrag.Dauer,
                Auftragsnummer = newAuftrag.Nummer,
                Kunde = kunde,
            };

            db.Auftraege.Add(auftrag);
            db.SaveChanges();

            foreach (var arbeit in newAuftrag.Arbeiter)
            {
                var arbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == arbeit.Id);

                var zeitarbieterauftrag = new ZeitarbeiterAuftrag
                {
                    Auftrag = auftrag,
                    Zeitarbeiter = arbeiter
                };

                db.ZeitarbeiterAuftraege.Add(zeitarbieterauftrag);
                db.SaveChanges();
            }

            return Ok();
        }
    }
}
